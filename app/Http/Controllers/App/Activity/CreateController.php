<?php

namespace App\Http\Controllers\App\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResourceRepository;
use App\Models\Activity;
use App\Models\ActivityMeta;

class CreateController extends Controller
{
	protected $request,$activity,$meta;

	public function __construct(Request $request,Activity $activity,ActivityMeta $meta)
	{
		$this->request = $request;
		$this->activity = new ResourceRepository($activity); 
	}

	public function index()
	{
		$data = [
			'name' => $this->request->name,
			'status' => 1
		];

		$save = $this->activity->create($data);

		return $save ? $this->success() : $this->error(); 
	} 

	public function success()
	{
		return back()
			->withNotification([
				'status' => 'success',
				'message' => 'Successfully added a new activity record !'
			]);
	}

	public function error()
	{
		return back()
			->withNotification([
				'status' => 'danger',
				'message' => 'Something went wrong !'
			]);
	}
}