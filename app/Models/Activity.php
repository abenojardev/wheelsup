<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Activity extends Authenticatable
{ 
	protected $table = 'activity';
	
    protected $fillable = [
        'name', 'status',
    ];
}
