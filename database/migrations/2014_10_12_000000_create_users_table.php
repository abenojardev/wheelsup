<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');  
            $table->string('status'); 
            $table->timestamps();
        });

        Schema::create('activity_meta', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('parent');  
            $table->string('meta_key');  
            $table->longText('meta_value')->nullable(); 
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
