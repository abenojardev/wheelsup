@extends('templates.layouts.asset.index')
@section('body')
    <body class="authentication-bg authentication-bg-pattern">   
        @yield('content')
        @include('templates.components.footer.index')
        <script src="{{ URL::asset('assets/js/vendor.min.js') }}"></script> 
        <script src="{{ URL::asset('assets/js/app.min.js') }}"></script>  
    </body>
@endsection