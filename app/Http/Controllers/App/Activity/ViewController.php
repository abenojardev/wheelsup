<?php

namespace App\Http\Controllers\App\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResourceRepository;
use App\Models\Activity;
use App\Models\ActivityMeta;

class ViewController extends Controller
{
	protected $request,$activity,$meta;

	public function __construct(Request $request,Activity $activity,ActivityMeta $meta)
	{
		$this->request = $request;
		$this->activity = new ResourceRepository($activity);
		$this->meta = new ResourceRepository($meta);
	}

	public function index()
	{
		$data = $this->activity->getAll();

		return view('pages.activity.index')
			->withData($data)
			->withTitle('Activities');
	} 
}