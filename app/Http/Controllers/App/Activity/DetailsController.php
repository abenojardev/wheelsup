<?php

namespace App\Http\Controllers\App\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResourceRepository;
use App\Models\Activity;
use App\Models\ActivityMeta;

class DetailsController extends Controller
{
	protected $request,$activity,$meta;

	public function __construct(Request $request,Activity $activity,ActivityMeta $meta)
	{
		$this->request = $request;
		$this->activity = new ResourceRepository($activity);
		$this->meta = new ResourceRepository($meta);
	}

	public function index($id)
	{
		$id = base64_decode($this->request->id);

		if (!$this->checker($id)) {
			return $this->error();
		} 

		$data = $this->arrangeData($id);
		
		return view('pages.activity.details')
			->withData($data)
			->withId($id)
			->withTitle('Activity Details');
	} 

	public function arrangeData($id)
	{
		$model = $this->meta->getAll([ 'parent' => $id]); 
		$data = [];

		if ($model) {
			foreach ($model as $key => $value) {
				$data[$value->meta_key] = [ 
					'id' => $value->id,
					'data' => json_decode($value->meta_value)
				];
			}
		}

		return $data;
	}

	public function checker($id)
	{
		return $this->activity->getById($id);
	}

	public function error()
	{
		return back()
			->withNotification([
				'status' => 'danger',
				'message' => 'Something went wrong !'
			]);
	}
}