<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.login.index')
    	->withTitle('Login');
})->name('app.login');
 
Route::get('/dashboard', function () {
    return view('pages.dashboard.index')
    	->withTitle('Dashboard');
})->name('app.dashboard');

Route::get('/flight', function () {
    return view('pages.flight.index')
    	->withTitle('Flights');
})->name('app.flight');

Route::get('/hotel', function () {
    return view('pages.hotel.index')
    	->withTitle('Hotels');
})->name('app.hotel');

Route::get('/uber', function () {
    return view('pages.uber.index')
        ->withTitle('Uber');
})->name('app.uber');

/* Dynamic */

Route::get('/activity')
    ->uses('App\Activity\ViewController@index')
    ->name('app.activity');

Route::post('/activity/add')
    ->uses('App\Activity\CreateController@index')
    ->name('app.activity.add');

Route::get('/activity/details/{id}')
    ->uses('App\Activity\DetailsController@index')
    ->name('app.activity.details');

Route::post('/activity/details/save/{id}')
    ->uses('App\Activity\SaveDetailsController@index')
    ->name('app.activity.details.save');
