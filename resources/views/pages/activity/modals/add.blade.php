<div id="add" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add {{ strtolower($title) }} form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form action="{{ URL::route('app.activity.add') }}" method="post">
                {{ csrf_field() }}
                <div class="modal-body p-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Activity Name</label>
                                <input type="text" class="form-control" name="name" required>
                            </div>
                        </div> 
                    </div> 
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success waves-effect waves-light">Save</button>
                    <a type="button" class="btn btn-light waves-effect" data-dismiss="modal">Close</a>
                </div>
            </form>
        </div>
    </div>
</div> 