@extends('templates.layouts.dashboard.index')
@section('content') 
	<div class="row">
	    <div class="col-12">
	        <div class="page-title-box"> 
	            <h4 class="page-title">{{ $title }}</h4>
	        </div>
	    </div>
	</div>     
	<div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <button type="button" class="btn btn-danger mb-2 mr-1"><i class="mdi mdi-delete"></i></button> 
                                <button type="button" class="btn btn-light mb-2">Update</button>
                                <button type="button" class="btn btn-light mb-2">Export</button>
                            </div>
                        </div><!-- end col-->
                    </div>

                    <div class="table-responsive">
                        <table class="table table-centeredx table-striped" id="products-datatable">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>ID</th>
                                    <th>Airline</th>
                                    <th>Departure</th>
                                    <th>Arrival</th> 
                                    <th>Boarding</th>
                                    <th>Connecting</th>
                                    <th>Seat</th>
                                    <th>Gate</th> 
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                                            <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td>
                                    	12001
                                    </td>
                                    <td>
                                    	<b>SouthWest</b> 
                                    	#AB1234
                                    </td> 
                                    <td>
                                    	<b>Austin-Bergstrom International Airport</b>
                                    	<p>10:30:00 AM CST</p>
                                    	<p>3600 Presidential Blvd, Austin, TX, 78719, United States</p>
                                    </td>
                                    <td>
                                    	<b>Reno-Tahoe International Airport</b>
                                    	<p>11:50:00 AM GMT</p>
                                    	<p>2001 E Plumb Ln, Reno, NV, 89502, United States</p>
                                    </td>
                                    <td>
                                    	<b>6/28/2019 10:00 AM</b><br>
                                        <p></p>
                                    	3h 20m
                                    </td>
                                    <td>
                                    	~
                                    </td> 
                                    <td>
                                    	~
                                    </td> 
                                    <td>
                                    	~
                                    </td> 
                                </tr>  
                            </tbody>
                        </table>
                    </div>

                    <ul class="pagination pagination-rounded justify-content-end mb-0">
                        <li class="page-item">
                            <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                <span aria-hidden="true">«</span>
                                <span class="sr-only">Previous</span>
                            </a>
                        </li>
                        <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                        <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                        <li class="page-item">
                            <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                <span aria-hidden="true">»</span>
                                <span class="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>

                </div> 
            </div> 
        </div>  
    </div>
@endsection 