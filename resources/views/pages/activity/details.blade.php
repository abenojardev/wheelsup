@extends('templates.layouts.dashboard.index')
@section('content') 
    <?php    
        function sortOut($key,$row,$data,$idget = false) {

            $id = null;
            $value = null;

            if(array_key_exists($key, $data)){
                $collections = $data[$key];
                $id = $collections['id'];

                if (!is_null($collections['data'])) { 
                    if(array_key_exists($row, $collections['data'])){
                        $value = $collections['data']->$row;
                    }  
                }
            }

            if($idget) { 
                return $id;
            } 

            return $value;
        }  
    ?>
    <div class="row">
        <div class="col-12">
            <div class="page-title-box"> 
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <div class="col-md-4">
            @if(session('notification'))
                <div class="alert alert-{{ session('notification')['status'] }} alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    {{ session('notification')['message'] }}
                </div>
            @endif
        </div>
    </div>     
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body"> 
                    <div class="card-box">  
                        <ul class="nav nav-tabs nav-bordered nav-justified">
                            <li class="nav-item">
                                <a href="#tab-1" data-toggle="tab" aria-expanded="false" class="nav-link active">
                                    Details
                                </a>
                            </li> 
                            <li class="nav-item">
                                <a href="#tab-2" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Hours
                                </a>
                            </li> 
<!--                             <li class="nav-item">
                                <a href="#tab-3" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Images
                                </a>
                            </li> -->
                            <li class="nav-item">
                                <a href="#tab-4" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Snow
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#tab-5" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Forecast
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#tab-6" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Runs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#tab-7" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Skies
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#tab-8" data-toggle="tab" aria-expanded="false" class="nav-link">
                                    Snowboard
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-1">
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                    <div class="row">
                                        <div class="col-lg-6"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('details','available',$data,true) }}">
                                            <input type="hidden" name="key" value="details">   

                                            <div class="form-group mb-3">
                                                <label for="example-select">Available</label>
                                                <select class="form-control" id="example-select" name="available"> 
                                                    <option @if(!is_null(sortOut('details','available',$data)) && sortOut('details','available',$data) == 'Yes') selected @endif>Yes</option> 
                                                    <option @if(!is_null(sortOut('details','available',$data)) && sortOut('details','available',$data) == 'No') selected @endif>No</option>  
                                                </select>
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Serving Airport</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','servingairport',$data) }}" name="servingairport">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="example-textarea">Address</label>
                                                <textarea class="form-control" id="example-textarea" rows="5" name="address">{{ sortOut('details','address',$data) }}</textarea>
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">City</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','city',$data) }}" name="city">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">State</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','state',$data) }}" name="state">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Zip</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','zip',$data) }}" name="zip">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Country</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','country',$data) }}" name="country">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Timezone</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','timezone',$data) }}" name="timezone">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Name</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','name',$data) }}" name="name">
                                            </div>    
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Phone</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','phone',$data) }}" name="phone">
                                            </div>  
                                        </div>  
                                        <div class="col-lg-6">
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Website</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','website',$data) }}" name="website">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Email</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','email',$data) }}" name="email">
                                            </div>  
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Trail Map</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','trailmap',$data) }}" name="trailmap">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Elevation</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','elevation',$data) }}" name="elevation">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Vertical Drop</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','verticaldrop',$data) }}" name="verticaldrop">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Acrs</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','acrs',$data) }}" name="acrs">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Snow Making Acrs</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','snowmakingacrs',$data) }}" name="snowmakingacrs">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Tax</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','tax',$data) }}" name="tax">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Fees</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','fees',$data) }}" name="fees">
                                            </div>   
                                            <div class="form-group mb-3">
                                                <label for="simpleinput">Total</label>
                                                <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('details','total',$data) }}" name="total">
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="tab-pane" id="tab-2">
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                    <div class="row">
                                        <div class="col-lg-12"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('hours','mon',$data,true) }}">
                                            <input type="hidden" name="key" value="hours">   

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>Monday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','mon_open',$data) }}" name="mon_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','mon_close',$data) }}" name="mon_close">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Tuesday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','tue_open',$data) }}" name="tue_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','tue_close',$data) }}" name="tue_close">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Wednesday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','wed_open',$data) }}" name="wed_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','wed_close',$data) }}" name="wed_close">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Thursday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','thu_open',$data) }}" name="thu_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','thu_close',$data) }}" name="thu_close">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Friday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','fri_open',$data) }}" name="fri_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','fri_close',$data) }}" name="fri_close">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Saturday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','sat_open',$data) }}" name="sat_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','sat_close',$data) }}" name="sat_close">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Sunday</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','sun_open',$data) }}" name="sun_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="time" id="simpleinput" class="form-control" value="{{ sortOut('hours','sun_close',$data) }}" name="sun_close">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="tab-pane" id="tab-3"> 
                                3
                            </div>
                            <div class="tab-pane" id="tab-4">
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                    <div class="row">
                                        <div class="col-lg-12"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('snow','mon',$data,true) }}">
                                            <input type="hidden" name="key" value="snow">   

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>Base</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Depth In</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snow','base_depthin',$data) }}" name="base_depthin">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Date</label> 
                                                        <input type="date" id="simpleinput" class="form-control" value="{{ sortOut('snow','base_date',$data) }}" name="base_date">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Last</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Depth In</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snow','last_depthin',$data) }}" name="last_depthin">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Date</label> 
                                                        <input type="date" id="simpleinput" class="form-control" value="{{ sortOut('snow','last_date',$data) }}" name="last_date">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>New</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Depth In</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snow','new_depthin',$data) }}" name="new_depthin">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Date</label> 
                                                        <input type="date" id="simpleinput" class="form-control" value="{{ sortOut('snow','new_date',$data) }}" name="new_date">
                                                        </select>
                                                    </div>
                                                </div> 
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="tab-pane" id="tab-5">
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                    <div class="row">
                                        <div class="col-lg-12"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('forecast','mon',$data,true) }}">
                                            <input type="hidden" name="key" value="forecast">   

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>5 Day</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Depth In</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('forecast','day_depthin',$data) }}" name="day_depthin">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Date</label> 
                                                        <input type="date" id="simpleinput" class="form-control" value="{{ sortOut('forecast','day_date',$data) }}" name="day_date">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Week</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Depth In</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('forecast','week_depthin',$data) }}" name="week_depthin">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Date</label> 
                                                        <input type="date" id="simpleinput" class="form-control" value="{{ sortOut('forecast','week_date',$data) }}" name="week_date">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div>
                                                
                                                <div class="col-md-12">
                                                    <h5>Month</h5>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Depth In</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('forecast','month_depthin',$data) }}" name="month_depthin">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-6">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Date</label> 
                                                        <input type="date" id="simpleinput" class="form-control" value="{{ sortOut('forecast','month_date',$data) }}" name="month_date">
                                                        </select>
                                                    </div>
                                                </div> 
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="tab-pane" id="tab-6">
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                    <div class="row">
                                                                                <div class="col-lg-12"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('runs','mon',$data,true) }}">
                                            <input type="hidden" name="key" value="runs">   

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h5>Bowl</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','bowl_open',$data) }}" name="bowl_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','bowl_close',$data) }}" name="bowl_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','bowl_total',$data) }}" name="bowl_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Terrain Park</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','terrainpark_open',$data) }}" name="terrainpark_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','terrainpark_close',$data) }}" name="terrainpark_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','terrainpark_total',$data) }}" name="terrainpark_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Double Pack</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','doublepack_open',$data) }}" name="doublepack_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','doublepack_close',$data) }}" name="doublepack_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','doublepack_total',$data) }}" name="doublepack_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Black</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','black_open',$data) }}" name="black_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','black_close',$data) }}" name="black_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','black_total',$data) }}" name="black_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Double Blue</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','doubleblue_open',$data) }}" name="doubleblue_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','doubleblue_close',$data) }}" name="doubleblue_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','doubleblue_total',$data) }}" name="doubleblue_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Blue</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','blue_open',$data) }}" name="blue_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','blue_close',$data) }}" name="blue_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','blue_total',$data) }}" name="blue_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Green</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','green_open',$data) }}" name="green_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','green_close',$data) }}" name="green_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','green_total',$data) }}" name="green_total">
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-12"><hr></div> 

                                                <div class="col-md-12">
                                                    <h5>Bunny</h5>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Open</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','bunny_open',$data) }}" name="bunny_open">
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Close</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','bunny_close',$data) }}" name="bunny_close">
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">  
                                                    <div class="form-group mb-3">
                                                        <label for="example-select">Total</label> 
                                                        <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('runs','bunny_bunny',$data) }}" name="bunny_bunny">
                                                        </select>
                                                    </div>
                                                </div> 
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="tab-pane" id="tab-7"> 
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                                                       <div class="row">
                                        <div class="col-lg-12"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('skies','mon',$data,true) }}">
                                            <input type="hidden" name="key" value="skies">   

                                            <div class="row">
                                                <?php  
                                                    $skies = [
                                                        ['name'=>'adult'],
                                                        ['name'=>'child'],
                                                        ['name'=>'senior'] 
                                                    ];
                                                ?>
                                                @foreach($skies as $v)
                                                    <div class="col-md-12">
                                                        <h5>{{ ucfirst($v['name']) }}</h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>1 Day</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_1day_beginner',$data) }}" name="{{ $v['name'] }}_1day_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_1day_standard',$data) }}" name="{{ $v['name'] }}_1day_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_1day_demo',$data) }}" name="{{ $v['name'] }}_1day_demo">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>3 Day</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_3day_beginner',$data) }}" name="{{ $v['name'] }}_3day_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_3day_standard',$data) }}" name="{{ $v['name'] }}_3day_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_3day_demo',$data) }}" name="{{ $v['name'] }}_3day_demo">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>5 Day</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_5day_beginner',$data) }}" name="{{ $v['name'] }}_5day_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_5day_standard',$data) }}" name="{{ $v['name'] }}_5day_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_5day_demo',$data) }}" name="{{ $v['name'] }}_5day_demo">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>Week</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_week_beginner',$data) }}" name="{{ $v['name'] }}_week_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_week_standard',$data) }}" name="{{ $v['name'] }}_week_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('skies',$v['name'].'_week_demo',$data) }}" name="{{ $v['name'] }}_week_demo">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"><hr></div>

                                                @endforeach 
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div>
                            <div class="tab-pane" id="tab-8">
                                <form action="{{ URL::route('app.activity.details.save',base64_encode($id)) }}" method="post">  
                                                                       <div class="row">
                                        <div class="col-lg-12"> 
                                            {{ csrf_field() }}
                                            <input type="hidden" name="type" value="{{ sortOut('snowboard','mon',$data,true) }}">
                                            <input type="hidden" name="key" value="snowboard">   

                                            <div class="row">
                                                <?php  
                                                    $snowboard = [
                                                        ['name'=>'adult'],
                                                        ['name'=>'child'],
                                                        ['name'=>'senior'] 
                                                    ];
                                                ?>
                                                @foreach($snowboard as $v)
                                                    <div class="col-md-12">
                                                        <h5>{{ ucfirst($v['name']) }}</h5>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>1 Day</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_1day_beginner',$data) }}" name="{{ $v['name'] }}_1day_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_1day_standard',$data) }}" name="{{ $v['name'] }}_1day_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_1day_demo',$data) }}" name="{{ $v['name'] }}_1day_demo">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>3 Day</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_3day_beginner',$data) }}" name="{{ $v['name'] }}_3day_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_3day_standard',$data) }}" name="{{ $v['name'] }}_3day_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_3day_demo',$data) }}" name="{{ $v['name'] }}_3day_demo">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>5 Day</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_5day_beginner',$data) }}" name="{{ $v['name'] }}_5day_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_5day_standard',$data) }}" name="{{ $v['name'] }}_5day_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_5day_demo',$data) }}" name="{{ $v['name'] }}_5day_demo">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <h6>Week</h6>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Beginner</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_week_beginner',$data) }}" name="{{ $v['name'] }}_week_beginner">
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Standard</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_week_standard',$data) }}" name="{{ $v['name'] }}_week_standard">
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">  
                                                        <div class="form-group mb-3">
                                                            <label for="example-select">Demo</label> 
                                                            <input type="text" id="simpleinput" class="form-control" value="{{ sortOut('snowboard',$v['name'].'_week_demo',$data) }}" name="{{ $v['name'] }}_week_demo">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12"><hr></div>

                                                @endforeach 
                                            </div>   
                                        </div>
                                        <div class="col-lg-12">
                                            <hr> 
                                            <button class="btn btn-success waves-effect waves-light pull-right">Save</button>
                                        </div>
                                    </div>
                                </form> 
                            </div> 
                        </div>
                    </div> 
                </div> 
            </div> 
        </div>  
    </div> 
@endsection 
