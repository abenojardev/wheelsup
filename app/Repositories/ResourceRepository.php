<?php 

namespace App\Repositories;
 
use Auth;
use Illuminate\Database\Eloquent\Model; 
use App\Repositories\ResourceInterface; 

class ResourceRepository implements ResourceInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	}

	public function getById($id){
		return $this->model->find($id) ? $this->model->find($id) : 0;
	}

	public function getAll($parameters = null,$unique = null)
	{
		if(is_null($parameters)){
			return $this->model->whereStatus(1)->orderBy('id','DESC')->get();	
		}
 
		if (is_null($unique)) {
			return $this->model->where(function($query) use($parameters){
				foreach ($parameters as $key => $row) {
					$query->where($key,'=',$row);
				}
	  		})->whereStatus(1)->orderBy('id','DESC')->get();
	  	} else {
			return $this->model->where(function($query) use($parameters){
				foreach ($parameters as $key => $row) {
					$query->where($key,'=',$row);
				}
	  		})->whereStatus(1)->where('id','!=',$unique)->orderBy('id','DESC')->get();
	  	}
	} 

	public function getOr($col,$parameters,$static = null)
	{
		if($static == null){ 
			return $this->model->where(function($query) use($col,$parameters){
					for ($i=0; $i < count($parameters); $i++) {
						$query->orWhere($col,'=',$parameters[$i]);
					}
		  		})->whereStatus(1)->get();
		}

		return $this->model->where(function($query) use($col,$parameters){
				for ($i=0; $i < count($parameters); $i++) {
					$query->orWhere($col,'=',$parameters[$i]);
				}
	  		})->whereStatus(1)->where($static['col'],'=',$static['value'])->get();
	}

	public function getAvailable()
	{
		$userId = Auth::user()->id;
		return $this->model->where(function($query) use($userId){
			$query->where('orderStatus','=','ready')->whereNull('driver_id')->whereStatus(1);
		})->orWhere(function($query) use($userId){
			$query->orWhere('orderStatus','=','pickup')->whereDriver_id($userId)->whereStatus(1);
		})->orWhere(function($query) use($userId){
			$query->orWhere('orderStatus','=','ready')->whereDriver_id($userId)->whereStatus(1);
		})->get();
	}

	public function getCount($parameters = null,$unique = null)
	{
		return $this->getAll($parameters,$unique) == false ? 0 : $this->getAll($parameters,$unique)->count(); 
	} 

	public function create(array $attributes)
	{ 	
        try{ 

			if($this->model->create($attributes)){
				return 1;
			} 

			return 0;

        } catch (\Illuminate\Database\QueryException $e){ 
        	return $e;
        } 
	}

	public function reports($params)
	{ 
		$query = $this->model;

		if (!is_null($params['from']) && !is_null($params['to'])) {
			$query = $query->whereBetween('created_at', array($params['from'],$params['to']));
		}

		if ($params['branch'] != 'all') { 
			$query = $query->whereBranch_id($params['branch']);
		}

		if (isset($params['driver'])) { 
			$query = $query->whereDriver_id($params['driver']);
		}

		if ($params['status'] != 'all') { 
			$query = $query->where('orderStatus','=',$params['status']);
		}

		$query = $query->get(); 
		$overall = 0;

		foreach ($query as $k => $v) {
			if ($params['product'] != 'all') {
				$products = json_decode($v->orders);
				$new = [];
				foreach ($products as $x => $y) {
					if($y->id == $params['product']){ 
						$overall = $overall + $y->total;
						array_push($new, $y);
					}
				} 
				$query[$k]->orders = json_encode($new); 
			} else { 
				$products = json_decode($v->orders);
				foreach ($products as $x => $y) {
					$overall = $overall + $y->total;
				}
			} 
		}
		
		return [
			'data' => $query,
			'total' => $overall
		];
	}

	public function update($id,array $attributes)
	{
		$resource = $this->model->find($id); 
		
		if (!$resource) {
			return 0;
		}   

        try{ 

			if($resource->update($attributes)){
				return 1;
			} 

			return 0;

        } catch (\Illuminate\Database\QueryException $e){ 
        	return $e;
        } 
	}

	public function delete($id)
	{

		return $this->update($id,[ 'status' => 0 ]);

	}
}