@extends('templates.layouts.asset.index')
@section('body')
    <body>     
  		<div id="wrapper">  
	        @include('templates.components.topbar.index')
	        @include('templates.navs.index')

            <div class="content-page"> 
                <div class="content"> 
                    <div class="container-fluid">
                    	@yield('content')
                    </div>
                </div>
            </div> 
		</div> 

        @include('templates.components.footer.main') 
        <style type="text/css">
            .pagination,.dataTables_filter{
                float: right;
            }
        </style>
        <script src="{{ URL::asset('public/assets/js/vendor.min.js') }}"></script> 
        @yield('extraJs') 
        <script src="{{ URL::asset('public/assets/js/app.min.js') }}"></script>  
    </body>
@endsection