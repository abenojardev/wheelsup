<?php

namespace App\Http\Controllers\App\Activity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ResourceRepository;
use App\Models\Activity;
use App\Models\ActivityMeta;

class SaveDetailsController extends Controller
{
	protected $request,$activity,$meta;

	public function __construct(Request $request,Activity $activity,ActivityMeta $meta)
	{
		$this->request = $request;
		$this->activity = new ResourceRepository($activity);
		$this->meta = new ResourceRepository($meta);
	}

	public function index($id)
	{
		$id = base64_decode($this->request->id);

		if (!$this->checker($id)) {
			return $this->error();
		} 

		if(is_null($this->request->type)){
			return $this->create($id);
		} else {
			return $this->save($id);
		}
	} 

	public function create($id)
	{
		$data = $this->request->except('_token','type','key');

		$save = $this->meta->create([
			'parent' => $id,
			'meta_key' => $this->request->key,
			'meta_value' => json_encode($data),
			'status' => 1
		]);

		if ($save) {
			return $this->success();
		}

		return $this->error();
	}

	public function save($id)
	{
		$data = $this->request->except('_token','type','key');

		$save = $this->meta->update($this->request->type,[
			'parent' => $id,
			'meta_key' => $this->request->key,
			'meta_value' => json_encode($data) 
		]);

		if ($save) {
			return $this->success();
		}

		return $this->error();
	}
 
	public function checker($id)
	{
		return $this->activity->getById($id);
	} 

	public function success()
	{
		return back()
			->withNotification([
				'status' => 'success',
				'message' => 'Successfully saved !'
			]);
	}

	public function error()
	{
		return back()
			->withNotification([
				'status' => 'danger',
				'message' => 'Something went wrong !'
			]);
	}
}