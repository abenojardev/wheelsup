<?php 

namespace App\Repositories; 

interface ResourceInterface {

	public function getById($id);
	
	public function getAll($parameters,$unique);

	public function getOr($col,$parameters,$static);

	public function getAvailable();

	public function getCount($parameter,$unique);

	public function create(array $attributes);

	public function reports(array $params);

	public function update($id,array $attributes);

	public function delete($id);

}