@extends('templates.layouts.dashboard.index')
@section('content') 
	<div class="row">
	    <div class="col-12">
	        <div class="page-title-box"> 
	            <h4 class="page-title">{{ $title }}</h4>
	        </div>
	    </div>
	</div>   
	<div class="row">
	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
	                        <i class="fe-users font-22 avatar-title text-primary"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">58,947</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Customers</p>
	                    </div>
	                </div>
	            </div> 
	        </div> 
	    </div>  

	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-success border-success border">
	                        <i class="fe-triangle font-22 avatar-title text-success"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">127</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Activities</p>
	                    </div>
	                </div>
	            </div>  
	        </div>  
	    </div>  

	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-info border-info border">
	                        <i class="fe-file-text font-22 avatar-title text-info"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">258</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Reservations</p>
	                    </div>
	                </div>
	            </div> <!-- end row-->
	        </div> <!-- end widget-rounded-circle-->
	    </div> <!-- end col-->

	    <div class="col-md-6 col-xl-3">
	        <div class="widget-rounded-circle card-box">
	            <div class="row">
	                <div class="col-6">
	                    <div class="avatar-lg rounded-circle bg-soft-warning border-warning border">
	                        <i class="fe-bell font-22 avatar-title text-warning"></i>
	                    </div>
	                </div>
	                <div class="col-6">
	                    <div class="text-right">
	                        <h3 class="text-dark mt-1"><span data-plugin="counterup">741</span></h3>
	                        <p class="text-muted mb-1 text-truncate">Notifications</p>
	                    </div>
	                </div>
	            </div>  
	        </div> 
	    </div>  
	</div> 

	<div class="row">
	    <div class="col-xl-4">
	        <div class="card-box">
	            <h4 class="header-title mb-3">Total Revenue</h4>

	            <div class="widget-chart text-center" dir="ltr">
	                <input data-plugin="knob" data-width="160" data-height="160" data-linecap=round data-fgColor="#f1556c" value="60" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".12"/>
	                <h5 class="text-muted mt-3">Total sales made today</h5>
	                <h2>$178</h2>

	                <p class="text-muted w-75 mx-auto sp-line-2">Traditional heading elements are designed to work best in the meat.</p>

	                <div class="row mt-3">
	                    <div class="col-4">
	                        <p class="text-muted font-15 mb-1 text-truncate">Target</p>
	                        <h4><i class="fe-arrow-down text-danger mr-1"></i>$7.8k</h4>
	                    </div>
	                    <div class="col-4">
	                        <p class="text-muted font-15 mb-1 text-truncate">Last week</p>
	                        <h4><i class="fe-arrow-up text-success mr-1"></i>$1.4k</h4>
	                    </div>
	                    <div class="col-4">
	                        <p class="text-muted font-15 mb-1 text-truncate">Last Month</p>
	                        <h4><i class="fe-arrow-down text-danger mr-1"></i>$15k</h4>
	                    </div>
	                </div>
	                
	            </div>
	        </div>  
	    </div>  
	    <div class="col-xl-8">
	        <div class="card-box">
	            <h4 class="header-title mb-3">Sales Analytics</h4>

	            <div id="sales-analytics" class="flot-chart mt-4 pt-1" style="height: 375px;"></div>
	        </div>  
	    </div>  
	</div>    
@endsection
@section('extraJs')  
	<script src="{{ URL::asset('public/assets/libs/jquery-knob/jquery-knob.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('public/assets/libs/jquery-sparkline/jquery-sparkline.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('public/assets/libs/flot-charts/flot-charts.min.js') }}" type="text/javascript"></script>
	<script src="{{ URL::asset('public/assets/js/pages/dashboard-1.init.js') }}" type="text/javascript"></script>
@endsection