<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ActivityMeta extends Authenticatable
{ 
	protected $table = 'activity_meta';

    protected $fillable = [
        'parent', 'meta_key', 'meta_value', 'status'
    ];
}
