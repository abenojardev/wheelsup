<?php 
    $menu = [];

    $menu['dashboard'] = [
        'title' => 'Dashboard',
        'icon'  => 'fe-airplay',
        'url'   => 'dashboard'
    ];
    $menu['flights'] = [
        'title' => 'Flights',
        'icon'  => 'fe-cloud',
        'url'   => 'flight'
    ];
    $menu['hotels'] = [
        'title' => 'Hotels',
        'icon'  => 'fe-home',
        'url'   => 'hotel'
    ];
    $menu['uber'] = [
        'title' => 'Uber',
        'icon'  => 'fe-truck',
        'url'   => 'uber'
    ];
    $menu['activities'] = [
        'title' => 'Activities',
        'icon'  => 'fe-triangle',
        'url'   => 'activity'
    ];
    $menu['customers'] = [
        'title' => 'Customers',
        'icon'  => 'fe-users',
        'url'   => 'login'
    ];
    $menu['notifications'] = [
        'title' => 'Notifications',
        'icon'  => 'fe-bell',
        'url'   => 'login'
    ]; 

    $menu['reservations'] = [
        'title' => 'Reservations',
        'icon'  => 'fe-file-text',
        'sub'   => [
            [
                'title' => 'On going' ,
                'url'   => 'login'
            ],
            [
                'title' => 'Pending' ,
                'url'   => 'login'
            ],
            [
                'title' => 'Cancelled' ,
                'url'   => 'login'
            ],
            [
                'title' => 'History' ,
                'url'   => 'login'
            ]
        ]
    ];

    $menu['reports'] = [
        'title' => 'Reports',
        'icon'  => 'fe-bar-chart-2',
        'sub'   => [
            [
                'title' => 'Customers' ,
                'url'   => 'login'
            ], 
            [
                'title' => 'Activities' ,
                'url'   => 'login'
            ],
            [
                'title' => 'Transactions' ,
                'url'   => 'login'
            ]
        ]
    ];

    $menu['settings'] = [
        'title' => 'Settings',
        'icon'  => 'fe-settings',
        'sub'   => [
            [
                'title' => 'API' ,
                'url'   => 'login'
            ], 
            [
                'title' => 'Account' ,
                'url'   => 'login'
            ],
            [
                'title' => 'Logout' ,
                'url'   => 'login'
            ]
        ]
    ];

?>
<div class="left-side-menu"> 
    <div class="slimscroll-menu"> 
        <div id="sidebar-menu"> 
            <ul class="metismenu" id="side-menu"> 
                <li class="menu-title">Navigation</li> 
                @foreach($menu as $section)
                    <li>
                        @if(isset($section['sub']))
                            <a href="javascript: void(0);">
                                <i class="{{ $section['icon'] }}"></i>
                                <span>{{ $section['title'] }}</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <ul class="nav-second-level" aria-expanded="false">
                                @foreach($section['sub'] as $sub)
                                    <li>
                                        <a href="{{ URL::route('app.'.$sub['url']) }}">{{ $sub['title'] }}</a>
                                    </li> 
                                @endforeach
                            </ul>
                        @else
                            <a href="{{ URL::route('app.'.$section['url']) }}">
                                <i class="{{ $section['icon'] }}"></i> 
                                <span>{{ $section['title'] }}</span>
                            </a>  
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="clearfix"></div> 
    </div>
</div>