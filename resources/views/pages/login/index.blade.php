@extends('templates.layouts.login.index')
@section('content')  
    <div class="account-pages mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5">
                    <div class="card bg-pattern"> 
                        <div class="card-body p-4"> 
                            <div class="text-center w-75 m-auto"> 
                                <h2>Lazy Traveler</h2> 
                                <p class="text-muted mb-4 mt-3">Enter your username and password to access admin panel.</p>
                            </div>

                            <form action="{{ URL::route('app.dashboard') }}" method="get"> 
                                <div class="form-group mb-3">
                                    <label for="emailaddress">Username</label>
                                    <input class="form-control" type="text" id="emailaddress" required="" placeholder="Enter your username">
                                </div> 
                                <div class="form-group mb-3">
                                    <label for="password">Password</label>
                                    <input class="form-control" type="password" required="" id="password" placeholder="Enter your password">
                                </div> 
                                <div class="form-group mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="checkbox-signin" checked>
                                        <label class="custom-control-label" for="checkbox-signin">Remember me</label>
                                    </div>
                                </div> 
                                <div class="form-group mb-0 text-center">
                                    <button class="btn btn-primary btn-block" type="submit"> Log In </button>
                                </div> 
                            </form> 
                        </div>  
                    </div>  
                </div>  
            </div> 
        </div> 
    </div>  
@endsection