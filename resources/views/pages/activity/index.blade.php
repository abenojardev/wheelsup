@extends('templates.layouts.dashboard.index')
@section('content') 
    <div class="row">
        <div class="col-12">
            <div class="page-title-box"> 
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <div class="col-md-4">
            @if(session('notification'))
                <div class="alert alert-{{ session('notification')['status'] }} alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    {{ session('notification')['message'] }}
                </div>
            @endif
        </div>
    </div>     
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8">
                            <div class="text-sm-right">
                                <button type="button" class="btn btn-success mb-2 mr-1 waves-effect waves-light" data-toggle="modal" data-target="#add">
                                    <i class="mdi mdi-pencil"></i>
                                </button>  
                                <button type="button" class="btn btn-light mb-2">Export</button>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-centeredx table-striped" id="products-datatable">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                        </div>
                                    </th>
                                    <th>ID</th>
                                    <th>Activity Name</th>
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Status</th>   
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data) 
                                    @foreach($data as $k => $v)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                    <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                                </div>
                                            </td>
                                            <td>
                                                0000{{ $v->id }}
                                            </td>
                                            <td>
                                                <b>
                                                    {{ $v->name }}
                                                </b>  
                                            </td>  
                                            <td>
                                                {{ $v->created_at->diffForHumans() }}
                                            </td> 
                                            <td>
                                                {{ $v->updated_at->diffForHumans() }}
                                            </td> 
                                            <td>
                                                <span class="badge bg-soft-success text-success">Active</span>
                                            </td>  
                                            <td>
                                                <div class="btn-group mb-2">
                                                    <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="mdi mdi-chevron-down"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item" href="{{ URL::route('app.activity.details',base64_encode($v->id)) }}">More Details</a>
                                                        <a class="dropdown-item" href="#">Update</a> 
                                                        <a class="dropdown-item" href="#">Delete</a> 
                                                    </div>
                                                </div> 
                                            </td>
                                        </tr> 
                                    @endforeach 
                                @endif
                            </tbody>
                        </table>
                    </div> 
                </div> 
            </div> 
        </div>  
    </div>

    @include('pages.activity.modals.add')
@endsection 
